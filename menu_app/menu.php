<?php
require_once('utils.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Menu {

    protected $MSISDN;
    protected $USSD_STRING;
    protected $EXTRA;
    protected $NEXT_LEVEL;
    protected $strDISPLAY;

    public final function __construct($MSISDN, $USSD_STRING) {
        $this->MSISDN = $MSISDN;
        $this->USSD_STRING = $USSD_STRING;

        if (isset($_SESSION['NEXT_LEVEL'])) {
            $this->EXTRA = $_SESSION['EXTRA'];
            $this->NEXT_LEVEL = $_SESSION['NEXT_LEVEL'];
        } else {
            $this->EXTRA = "0";
            $this->NEXT_LEVEL = "0";
        }
    }

    public static function createInstance($MSISDN, $USSD_STRING) {
        $DMENU = new self($MSISDN, $USSD_STRING);
        return $DMENU;
    }


    public function processMenu() {

        try {

            $nav = "menuLevel" . "$this->NEXT_LEVEL";

            $this->$nav();

            $outputArray = split("\|", $this->strDISPLAY);

            $menu = $outputArray[0];
            $_SESSION["NEXT_LEVEL"] = $outputArray[1];
            $_SESSION["EXTRA"] = $outputArray[2];

        } catch (Exception $ex) {
            $menu = "Sorry Value Service is currently unavailable. Kindly Bare with us as we work on fixing this issue";
        }

        return $menu;
    }




    private function menuLevel0() {

        $this->strDISPLAY = "Welcome to Mswali \n1.Sign up\n2.How to play\n3.Practice Questions\n4.Leadersboard\n5.T&C\6.Play|1|EXTRA";

    }


    # Checking the root menu
    private function menuLevel1() {
        if ($this->USSD_STRING == "1") {
            $this->menuLevel2();
        } else if ($this->USSD_STRING == "2") {
            $this->menuLevel3();        
        } else if ($this->USSD_STRING == "3") {
            $this->menuLevel4();
        } else if ($this->USSD_STRING == "4") {
            $this->menuLevel5();
        } else if ($this->USSD_STRING == "5") {
            $this->menuLevel6();
        } else if ($this->USSD_STRING == "6") {
            $this->menuLevel7();
        } else {
            $this->menuLevel0();
        }
    }


    private function menuLevel2() {

        $userInfo = json_decode(getUser($MSISDN), true);

        //if user does not have an account
        if($userInfo['status']==404){

            $this->strDISPLAY = "Enter preferred username to register|0|EXTRA";

            $data_array = array(
                "name" => $this->USSD_STRING,
                "account_number" => $this->MSISDN
            );

            $register_user = postUser($data_array);
            
            }else{

                $this->strDISPLAY = "You are already registered. \n1.Sign up\n2.How to play\n3.Practice Questions\n4.Leadersboard\n5.T&C\6.Play|1|EXTRA";
            }

    }

    private function menuLevel3() {
        $this->strDISPLAY = "https://www.mswali.co.ke/How-to-play|0|EXTRA";
    }


    // private function menuLevel4() {
    //     $this->strDISPLAY = "https://www.mswali.co.ke/leadersboard|0|EXTRA";
    // }


    private function menuLevel5() {
        $this->strDISPLAY = "https://www.mswali.co.ke/leadersboard|0|EXTRA";
    }

    private function menuLevel6() {

        $this->strDISPLAY = "https://www.mswali.co.ke/T-and-Cs|0|EXTRA";
    }


    private function menuLevel7() {


        // check if user has enough funds to play
        $userInfo = json_decode(getUser($MSISDN), true);

        if($userInfo['data']['balance'] >= 20){
            $questions = fetchQuestions();

            $counter = 1;
            
            foreach($questions['data'] as $question){

                $this->strDISPLAY = $counter.". ".$question['title']."\n";

                $answers = getAnswers($question['id']);

                $answerArray = array();

                foreach ($answers as $key => $value) {

                    $this->strDISPLAY = $answerArray['choice'].". ".$answerArray['answer_text']."\n";
                }
            }

            } else {
                // send prompt user to top up the balance
                $this->strDISPLAY = "Sorry, you do not have sufficient funds. please load your wallet to continue";

        }

    }


    private function menuLevel8() {
        // function to check if the answer is right
        $this->strDISPLAY = "serve the next question||EXTRA";
    }


    // private function menuLevel9() {

    //     // check if selected answer is correct
    //     if ($this->USSD_STRING  == $answer->answer_text && $answer->correct==true) {
    //         $this->menuLevel5();
    //     } else if ($this->USSD_STRING == "2") {
    //         $this->menuLevel9();
    //     } else {
    //         $this->strDISPLAY = "Please try your chnace in the next session";
    //     }
    // }

}
