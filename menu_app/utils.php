<?php
error_reporting(E_ALL);
ini_set("display_errors", 0);
// $county = json_decode(file_get_contents('http://localhost/bliss/api/fetch-all-sub-counties/?api_key=222&county_id=1'));

// handle GET requests
function getData($url){
  
    $curl = curl_init();
    $headers = array(
        'Accept: application/json',
        'Content-type: application/json',
    );
    
    curl_setopt_array($curl, [

        // CURLOPT_HTTPHEADER => $headers,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Codular Sample cURL Request'
    ]);

    $resp = curl_exec($curl);

    curl_close($curl);

    return $resp;   

}

// function to hundle POST requests
function postData($url, $data){
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Codular Sample cURL Request',
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $data
        // CURLOPT_POSTFIELDS => [
        //     name => $name,
        //     msisdn => $msissdn
        // ]
    ]);
    $resp = curl_exec($curl);
    curl_close($curl);

    return $resp;

}


function postUser($data){

  // $name = $data['name'];
  // $account_number = $data['account_number'];
  
  // $url = 'http://localhost/mswali/api/post-user?r=users/name='.$name.'&account_number='.$account_number;

  $url = 'http://localhost/mswali/api/post-user?r=users/';

  $userInstance = postData($url, $data);

  if($userInstance){

    return "congratulation. Karibu Mswali";
      
  }

}



function getUser($account_number){
 
  $url = 'http://localhost/mswali/mswali_app/backend/web/index.php?r=api/get-user&account_number='.$account_number;

  return getData($url);
}


function getQuestion($question_id){
 
  $url = 'http://localhost/mswali/mswali_app/backend/web/index.php?r=api/get-question&question_id='.$question_id;

  return getData($url);
}


function fetchQuestions(){
 
  $url = 'http://localhost/mswali/mswali_app/backend/web/index.php?r=api/fetch-questions';

  return getData($url);
}

function getAnswers($question_id){
 
  $url = 'http://localhost/mswali/mswali_app/backend/web/index.php?r=api/fetch-answers&question_id='.$question_id;

  return getData($url);
}

function getWallet($user_id){
 
  $url = 'http://localhost/mswali/mswali_app/backend/web/index.php?r=api/get-wallet&user_id='.$user_id;

  return getData($url);
}