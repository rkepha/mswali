<?php

namespace backend\controllers;
// header("Access-Control-Allow-Origin: *");
// date_default_timezone_set('Africa/Nairobi');

use Yii;


use yii\db\Command;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use backend\models\Users;
use backend\models\Questions;
use backend\models\Answers;
use backend\models\Payments;
use backend\models\GameSessions;

use yii\db\Expression; // to randomise objects selected from the database



use yii\rest\ActiveController;


/**
 * ApiController implements the api actions
 */
class ApiController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        // 'actions' => [
                        //     'fetch-questions',
                        //     'get-question',
                        //     'fetch-answers,',
                        //     'get-wallet,',
                        //     'get-sessions,',
                        // ],
                        'allow' => true,
                        // 'role' => ['@', '?']
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\NotFoundHttpException('The resource you are looking for cannot be found');
                }
     
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'fetch-questions' => ['GET'],
                    'get-question' => ['GET'],
                    'get-user' => ['GET'],
                    'fetch-answers' => ['GET'],
                    'get-wallet' => ['GET'],
                    'get-sessions' => ['GET'],
                    'get-balance' => ['GET'],
                ],
            ],
            
        ];
    }


    /**
     * @inheritdoc
     */
    public function beforeAction($action) {            
        if ($action->id == 'get-question' || $action->id == 'fetch-questions' || $action->id == 'fetch-answers' || $action->id == 'get-wallet'
             || $action->id == 'get-sessions' || $action->id == 'get-user' || $action->id == 'get-balance')
        {
            $this->enableCsrfValidation = false;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        }

        return parent::beforeAction($action);
    }



   
    public function actionFetchQuestions() {



        $questionsList = Yii::$app->db->createCommand(
                                "SELECT * FROM (
                                    (SELECT * FROM questions WHERE active=true AND category = 'Kenya' ORDER BY RAND() LIMIT 5)
                                    UNION
                                    (SELECT * FROM questions WHERE active=true AND category = 'Africa' ORDER BY RAND() LIMIT 3)
                                    UNION
                                    (SELECT * FROM questions WHERE active=true AND category = 'World'  ORDER BY RAND() LIMIT 2)
                                    
                                ORDER BY RAND() ) 
                                collection;"
        
        )
        ->queryAll();

        // $kenyaQuestions = Questions::find()-                                                                                                                                                                                                                                                                                                                                                                                                     q>where(['active' => true, 'category' => 'Kenya' ])->orderBy(new Expression('rand()'))->limit((int)5,(int)100)->all();
        // $africaQuestions = Questions::find()->where(['active' => true, 'category' => 'Africa' ])->orderBy(new Expression('rand()'))->limit((int)3,(int)100)->all();
        // $worldQuestions = Questions::find()->where(['active' => true, 'category' => 'World' ])->orderBy(new Expression('rand()'))->limit((int)2,(int)100)->all();
        
        // $questionsList = $kenyaQuestions + $africaQuestions + $worldQuestions;
        
        if($questionsList){

            $data =  array(
                'status' => 200,
                'status_message' => count($questionsList). " questions found",
                'data' => $questionsList,
            );

            return $data;

            } else {
            $data =  array(
                'status' => 404,
                'status_message' => 'no Questions found',
            );
            return $data;
        }
   
    }

    public function actionGetQuestion() {

        $questionID = NULL;

  
        if(isset($_GET['question_id'])) {
            $questionID = $_GET['question_id'];
            } else {
                $data =  array(
                    'status' => 403,
                    'status_message' => 'missing question_id'
                );
                return $data;
        }

        $question = Questions::find()->where(['id' => $questionID])->one();
        
    
        if(empty($question)){
            $data =  array(
                'status' => 222,
                'status_message' => 'No questions found',
            );
            return $data;
        }
        return $question;
    
    }

    
    public function actionFetchAnswers() {
      
        $question_id = NULL;

        if(isset($_GET['question_id'])) {
            $question_id = $_GET['question_id'];
            } else {
                $data =  array(
                'status' => 403,
                'status_message' => 'missing question_id'
                );
                return $data;
        }

        $answers = Answers::find()->where(['id' => $question_id])->all();
        
        if($answers){
            return $answers;
            }else {
                $data =  array(
                    'status' => 404,
                    'status_message' => 'No answers found',
                );
            return $data;
        }

    }


    public function actionGetBalance() {
      
        $account = NULL;

        if(isset($_GET['account'])) {
            $account = $_GET['account'];
            } else {
                $data =  array(
                'status' => 200,
                'status_message' => 'missing account number'
                );
                return $data;
        }

        $userBalance = Yii::$app->db->createCommand(
                        "SELECT name, amount FROM users AS u 
                        INNER JOIN wallet AS w ON u.id = w.user_id
                        WHERE u.account_number=$account"
        )
        ->queryOne();

        if($userBalance){
            return $userBalance;
            }else {
                $data =  array(
                    'status' => 404,
                    'status_message' => 'No user with this account found',
                );     

            return $data;
        }

    }

    public function actionGetUser() {
      
        $accountNumber = NULL;

        if(isset($_GET['account_number'])) {
            $accountNumber = $_GET['account_number'];
            } else {
                $data =  array(
                    'status' => 403,
                    'status_message' => 'missing account_number'
                );
                return $data;
        }

        $user = Users::find()->where(['account_number' => $accountNumber])->one();
        
        if(count($user)>0){
            return $data =  array(
                        'status' => 200,
                        'status_message' => 'user successfully found',
                        'data' => $user
                    );
            }else {
                $data =  array(
                    'status' => 404,
                    'status_message' => 'No user found',
                );
            return $data;
        }

    }
    public function actionGetSessions() {
      
        $question_id = NULL;

        if(isset($_GET['api_key'])) {
                $question_id = $_GET['question_id'];
            } else {
                $data =  array(
                'status' => 403,
                'status_message' => 'missing question_id'
                );
                return $data;
        }

        $sessions = GameSessions::find()->all();
        
        if($sessions){
            return $sessions;
            }else {
                $data =  array(
                    'status' => 404,
                    'status_message' => 'No answers found',
                );
            return $data;
        }

    }
} 
