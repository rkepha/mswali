<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "administrators".
 *
 * @property int $id
 * @property string $name
 *
 * @property Questions[] $questions
 */
class Administrators extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'administrators';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Questions::className(), ['inserted_by' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AdministratorsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AdministratorsQuery(get_called_class());
    }
}
