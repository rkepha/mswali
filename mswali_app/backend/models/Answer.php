<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "answers".
 *
 * @property int $id
 * @property string $answer_text
 * @property string $inserted_at
 * @property string $updated_at
 * @property int $correct
 * @property int $question_id
 *
 * @property Question $question
 */
class Answer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'correct', 'question_id'], 'integer'],
            [['inserted_at', 'updated_at'], 'safe'],
            [['answer_text'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'answer_text' => 'Answer Text',
            'inserted_at' => 'Inserted At',
            'updated_at' => 'Updated At',
            'correct' => 'Correct',
            'question_id' => 'Question ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    /**
     * {@inheritdoc}
     * @return AnswerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AnswerQuery(get_called_class());
    }
}
