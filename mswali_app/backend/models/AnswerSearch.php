<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Answers;

/**
 * AnswerSearch represents the model behind the search form of `backend\models\Answers`.
 */
class AnswerSearch extends Answers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'correct', 'question_id'], 'integer'],
            [['answer_text', 'choice', 'inserted_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Answers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'inserted_at' => $this->inserted_at,
            'updated_at' => $this->updated_at,
            'correct' => $this->correct,
            'question_id' => $this->question_id,
        ]);

        $query->andFilterWhere(['like', 'answer_text', $this->answer_text]);

        return $dataProvider;
    }
}
