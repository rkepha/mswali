<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "answers".
 *
 * @property int $id
 * @property string $answer_text
 * @property string $inserted_at
 * @property string $updated_at
 * @property int $correct
 * @property int $question_id
 *
 * @property Questions $question
 */
class Answers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'correct', 'question_id'], 'integer'],
            [['inserted_at', 'updated_at'], 'safe'],
            [['answer_text', 'choice'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'answer_text' => 'Answer Option',
            'inserted_at' => 'Inserted At',
            'updated_at' => 'Updated At',
            'choice' => 'Choice',
            'correct' => 'Correct',
            'question_id' => 'Question Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['id' => 'question_id']);
    }

    /**
     * {@inheritdoc}
     * @return AnswersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AnswersQuery(get_called_class());
    }
}
