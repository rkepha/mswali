<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "game_sessions".
 *
 * @property int $id
 * @property int $player_count
 * @property int $winner_count
 *
 * @property Leaderboard[] $leaderboards
 * @property Payments[] $payments
 */
class GameSessions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'game_sessions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'player_count', 'winner_count'], 'integer'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'player_count' => 'Player Count',
            'winner_count' => 'Winner Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeaderboards()
    {
        return $this->hasMany(Leaderboard::className(), ['session_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['session_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return GameSessionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameSessionsQuery(get_called_class());
    }
}
