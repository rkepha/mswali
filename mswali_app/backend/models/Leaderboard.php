<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "leaderboard".
 *
 * @property int $id
 * @property int $user_id
 * @property string $inserted_at
 * @property int $session_id
 *
 * @property User $user
 * @property GameSession $session
 * @property Administrator $user0
 */
class Leaderboard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'leaderboard';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'user_id', 'session_id'], 'integer'],
            [['inserted_at'], 'safe'],
            [['id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['session_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSession::className(), 'targetAttribute' => ['session_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Administrator::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'inserted_at' => 'Inserted At',
            'session_id' => 'Session ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(GameSession::className(), ['id' => 'session_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(Administrator::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return LeaderboardQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LeaderboardQuery(get_called_class());
    }

}
