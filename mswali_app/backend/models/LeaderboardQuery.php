<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Leaderboard]].
 *
 * @see Leaderboard
 */
class LeaderboardQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Leaderboard[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Leaderboard|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
