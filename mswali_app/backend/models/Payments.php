<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property string $amount
 * @property string $paid_at
 * @property int $session_id
 * @property int $user_id
 *
 * @property GameSessions $session
 * @property Users $user
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'session_id', 'user_id'], 'integer'],
            [['paid_at'], 'safe'],
            [['amount'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['session_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSessions::className(), 'targetAttribute' => ['session_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount' => 'Amount',
            'paid_at' => 'Paid At',
            'session_id' => 'Session ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(GameSessions::className(), ['id' => 'session_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return PaymentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentsQuery(get_called_class());
    }
}
