<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "questions_performance".
 *
 * @property int $id
 * @property int $question_id
 * @property int $session_id
 * @property int $correct_answer_count
 * @property int $wrong_answer_count
 * @property int $players_count
 *
 * @property Question $question
 * @property GameSession $session
 */
class QuestionPerformance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions_performance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'question_id', 'session_id', 'correct_answer_count', 'wrong_answer_count', 'players_count'], 'integer'],
            [['id'], 'unique'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
            [['session_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSession::className(), 'targetAttribute' => ['session_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'session_id' => 'Session ID',
            'correct_answer_count' => 'Correct Answer Count',
            'wrong_answer_count' => 'Wrong Answer Count',
            'players_count' => 'Players Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(GameSession::className(), ['id' => 'session_id']);
    }

    /**
     * {@inheritdoc}
     * @return QuestionPerformanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuestionPerformanceQuery(get_called_class());
    }
}
