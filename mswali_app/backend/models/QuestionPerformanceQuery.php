<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[QuestionPerformance]].
 *
 * @see QuestionPerformance
 */
class QuestionPerformanceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return QuestionPerformance[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return QuestionPerformance|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
