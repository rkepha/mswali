<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property string $title
 * @property string $category
 * @property string $topic
 * @property string $inserted_at
 * @property int $active
 * @property string $updated_at
 * @property int $inserted_by
 *
 * @property Answers[] $answers
 * @property Administrators $insertedBy
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'active', 'inserted_by'], 'integer'],
            [['inserted_at', 'updated_at'], 'safe'],
            [['title', 'category', 'topic'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['inserted_by'], 'exist', 'skipOnError' => true, 'targetClass' => Administrators::className(), 'targetAttribute' => ['inserted_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'category' => 'Category',
            'topic' => 'Topic',
            'inserted_at' => 'Inserted At',
            'active' => 'Active',
            'updated_at' => 'Updated At',
            'inserted_by' => 'Inserted By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answers::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInsertedBy()
    {
        return $this->hasOne(Administrators::className(), ['id' => 'inserted_by']);
    }

    /**
     * {@inheritdoc}
     * @return QuestionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuestionsQuery(get_called_class());
    }
}
