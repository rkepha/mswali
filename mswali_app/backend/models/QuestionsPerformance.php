<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "questions_performance".
 *
 * @property int $id
 * @property int $question_id
 * @property int $session_id
 * @property int $correct_answer_count
 * @property int $wrong_answer_count
 * @property int $players_count
 *
 * @property Questions $question
 * @property GameSessions $session
 */
class QuestionsPerformance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions_performance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'question_id', 'session_id', 'correct_answer_count', 'wrong_answer_count', 'players_count'], 'integer'],
            [['id'], 'unique'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['question_id' => 'id']],
            [['session_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSessions::className(), 'targetAttribute' => ['session_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'session_id' => 'Session ID',
            'correct_answer_count' => 'Correct Answer Count',
            'wrong_answer_count' => 'Wrong Answer Count',
            'players_count' => 'Players Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(GameSessions::className(), ['id' => 'session_id']);
    }

    /**
     * {@inheritdoc}
     * @return QuestionsPerformanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuestionsPerformanceQuery(get_called_class());
    }
}
