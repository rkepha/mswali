<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[QuestionsPerformance]].
 *
 * @see QuestionsPerformance
 */
class QuestionsPerformanceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return QuestionsPerformance[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return QuestionsPerformance|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
