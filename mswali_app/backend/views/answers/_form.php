<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Questions;


/* @var $this yii\web\View */
/* @var $model backend\models\Answers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="answers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'answer_text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'inserted_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'choice')->textInput() ?>

    <?= $form->field($model, 'correct')->textInput() ?>

    <?= $form->field($model, 'question_id')->dropDownList(
        ArrayHelper::map(Questions::find()->orderBy('title')->asArray()->all(),'id','title'),
        ['prompt'=>'Select question']

) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
