<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\QuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index" style="margin: 20px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p> 
        <span style="float:right;margin-right: 100px; margin-bottom: 20px;">
            <?= Html::a('Add Question', ['create'], ['class' => 'btn btn-success']) ?>
        </span>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'category',
            'topic',
            'inserted_at',
            //'active',
            //'updated_at',
            //'inserted_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
